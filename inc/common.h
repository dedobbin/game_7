#pragma once
#include <vector>
#include <string>
#include <stdio.h>
#include <dirent.h>
#include "loggger.h"

namespace Common {
    inline std::vector<std::string> get_file_names(std::string path)
    {
        std::vector<std::string> files;

        const char *directory_path = path.c_str();

        DIR *dir = opendir(directory_path);
        if (dir == NULL) {
           LOGGGER_ERROR("Error opening directory {}", directory_path);
            return {};
        }

        struct dirent *entry;
        while ((entry = readdir(dir)) != NULL) {
            if (entry->d_type == DT_REG) { 
                files.push_back(entry->d_name);
            }
        }
        closedir(dir);
        return files;
    }
}