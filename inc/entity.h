#pragma once

#include <tuple>
#include <unordered_map>
#include "sprite.h"

enum class Dir {
    UP,
    DOWN,
    LEFT,
    RIGHT,
};

class Entity 
{
public:
    Entity (Sprite sprite, int x, int y, int w=32, int h=32);
    Entity (std::unordered_map<std::string, Sprite> sprites, int x, int y, int w=32, int h=32);
    bool move(Dir dir, float delta_time = 1);
    bool set_active_sprite(const std::string& sprite_name);
    std::tuple<int, int, int, int> get_pos();
    Sprite sprite;
private:
    std::unordered_map<std::string, Sprite> sprites;
    float x;
    float y;
    float w;
    float h;
    const int speed = 3;
};