#pragma once

#include <memory>
#include <unordered_map>
#include <map>
#include <vector>
#include <functional>
#include <string>
#include "sprite.h"
#include "sdl_deleters.h"
#include "text.h"
#include "SDL.h"
#include "SDL_ttf.h"


#define FPS 60
#define FRAME_DELAY 1000 / FPS

#define FONT_PTSIZE 28
#define FONT_PATH "../assets/font.ttf"

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

class SDL 
{
public:
    SDL();
    ~SDL();
    static SDL* get();
    void start_game(
        std::function<void(SDL* sdl)> render_callback,
        std::function<bool(SDL* sdl, const SDL_Event& event, float delta_time)> input_callback 
    );
    void load_textures(std::string folder);
    void set_view_port(int x, int y, int w, int h);
    bool add_font(const std::string name, const std::string &path, int ptsize=28);
    void render_sprite(Sprite &sprite, int x, int y, int w, int h);
    void render_text(const std::string &text, int x, int y, const std::string &font="default", uint8_t r=255, uint8_t g=255, uint8_t b=255);

    int add_static_text(const std::string &text, int x, int y, bool word_for_word = false, const std::string &font = "default", uint8_t r =255, uint8_t g=255, uint8_t b=255);
    bool remove_text(int key);

    float get_fps() const;
    //TODO: should be private
    std::unordered_map<std::string, std::unique_ptr<TTF_Font, TTF_font_deleter>> fonts;
private:
    void render_static_texts();
    std::unique_ptr<SDL_Window, SDL_window_deleter> window;
    std::unique_ptr<SDL_Renderer, SDL_renderer_deleter> renderer;
    std::unordered_map<std::string, SDL_Rect> viewports;
    std::unordered_map<std::string, std::unique_ptr<SDL_Texture, SDL_texture_deleter>> textures;
    std::map<int, std::unique_ptr<Text>> texts;
    float fps = 0;
};