#pragma once

#include <memory>
#include "sdl_deleters.h"

class Text {
public:
    Text(SDL_Texture* texture, SDL_Rect pos);
    virtual void tick();
    SDL_Rect* get_pos_raw();
    const std::unique_ptr<SDL_Texture, SDL_texture_deleter> texture;
protected:
    SDL_Rect pos; 
};

class WordForWordText : public Text {
public:
    WordForWordText(SDL_Texture* texture, SDL_Rect pos, std::string text, std::string font);
    // Shadow text so we can display it word for word
    const std::string text;
    const std::string font;
    const int original_w;
    const int ticks_per_word = 40;
    void tick() override;
private:
    int next_word();
    int cur_tick;
    std::string cur_text;
};