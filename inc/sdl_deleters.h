#pragma once

#include "SDL.h"
#include "SDL_ttf.h"

struct SDL_window_deleter {
    void operator()(SDL_Window* window) const;
};

struct SDL_renderer_deleter {
    void operator()(SDL_Renderer* renderer) const;
};

struct SDL_texture_deleter {
    void operator()(SDL_Texture* texture) const;
};

struct TTF_font_deleter {
    void operator()(TTF_Font* font) const;
};