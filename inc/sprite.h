#pragma once
#include <string>
#include <vector>

struct AnimationFrame
{
    const int x;
    const int y;
    const int w;
    const int h;
    const int duration; // in frame_ticks
};

class Sprite 
{
public:
    // This will use texture_str to look up the texture. 
    // Would be faster to pass SDL_Texture directly, as shared ptr, but would couple hard.
    // If it proves to be a bottleneck, we can change it.
    Sprite(std::string texture_str, int x, int y, int w, int h);
    Sprite(std::string texture_str, std::vector<AnimationFrame> animation_frames);
    Sprite();
    Sprite& operator=(const Sprite &);
    void tick();
    std::string get_texture_str() const;
    int getX() const;
    int getY() const;
    int getW() const;
    int getH() const;

private:
    std::string texture_str;
    std::vector<AnimationFrame> animation_frames;
    int cur_frame_animation = 0;
    int cur_frame_tick = 0;
    int x;
    int y;
    int w;
    int h;
};