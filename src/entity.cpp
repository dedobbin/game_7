#include "entity.h"

Entity::Entity (Sprite sprite, int x, int y, int w, int h) :
    sprite(sprite), x(x), y(y), w(w), h(h)
{
    this->sprite = sprite;
    this->x = x;
    this->y = y;
    this->w = w;
    this->h = h;
    sprites.insert(std::make_pair("default", sprite));
}

Entity::Entity (std::unordered_map<std::string, Sprite> sprites, int x, int y, int w, int h)
{
    if (sprites.size() == 0) {
        throw std::invalid_argument("sprites must have at least one sprite");
    }
    this->sprites = sprites;
    this->x = x;
    this->y = y;
    this->w = w;
    this->h = h;

    if (sprites.find("default") != sprites.end()) {
        this->sprite = sprites["default"];
    } else {
        this->sprite = sprites.begin()->second;
    }
}

bool Entity::move(Dir dir, float delta_time)
{
    const float multiplier = delta_time * 100;
    switch (dir) {
        case Dir::UP:
            y -= speed * multiplier;
            break;
        case Dir::DOWN:
            y += speed * multiplier;
            break;
        case Dir::LEFT:
            x -= speed * multiplier;
            break;
        case Dir::RIGHT:
            x += speed * multiplier;
            break;
    }
    return true;
}

bool Entity::set_active_sprite(const std::string& sprite_name)
{
    if (sprites.find(sprite_name) == sprites.end()) {
        return false;
    }
    sprite = sprites[sprite_name];
    return true;
}


std::tuple<int, int, int, int> Entity::get_pos()
{
    return std::make_tuple(x, y, w, h);
}