#include "sdl_deleters.h"
#include "loggger.h"

void SDL_window_deleter::operator()(SDL_Window* window) const {
    if (window != nullptr) {
        LOGGGER_TRACE("Deleting SDL_Window");
        SDL_DestroyWindow(window);
    }
}

void SDL_renderer_deleter::operator()(SDL_Renderer* renderer) const {
    if (renderer != nullptr) {
        LOGGGER_TRACE("Deleting SDL_Renderer");
        SDL_DestroyRenderer(renderer);
    }
}

void SDL_texture_deleter::operator()(SDL_Texture* texture) const {
    if (texture != nullptr) {
        LOGGGER_TRACE("Deleting SDL_Texture");
        SDL_DestroyTexture(texture);
    }
}

void TTF_font_deleter::operator()(TTF_Font* font) const {
    if (font != nullptr) {
        LOGGGER_TRACE("Deleting TTF_Font");
        //TODO: currently does nothing. Could be situation with more than 1 font, 
        //  so we don' call TTF_Quit
        //TTF_CloseFont(font);
    }
}