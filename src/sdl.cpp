#include "sdl.h"
#include "SDL_image.h"
#include "common.h"

static SDL sdl;

SDL_Texture* load_texture(std::string path, SDL_Renderer* renderer){
	LOGGGER_TRACE("Loading texture {}", path);
    SDL_Surface* loaded_surface = IMG_Load( path.c_str() );
	if( loaded_surface == NULL ){
       LOGGGER_ERROR("Unable to load image {}! SDL_image Error: {}", path.c_str(), IMG_GetError());
        return nullptr;
    }

    if(SDL_SetColorKey(loaded_surface, SDL_TRUE, SDL_MapRGB(loaded_surface->format, 0x22, 0xB1, 0x4C)) != 0){
       LOGGGER_ERROR("Unable to set colourkey: {}", SDL_GetError());
    }

    SDL_Texture* new_texture = SDL_CreateTextureFromSurface(renderer, loaded_surface );
    if( new_texture == NULL ){
       LOGGGER_ERROR("Unable to create texture from {}! SDL Error: {}", path.c_str(), SDL_GetError());
        return nullptr;
    }

    SDL_FreeSurface(loaded_surface);
	return new_texture;
}

SDL* SDL::get(){
    return &sdl;
}

SDL::SDL()
{
    LOGGGER_TRACE("SDL::init()");
	if (SDL_Init(SDL_INIT_VIDEO) < 0 ){
       LOGGGER_ERROR("SDL could not initialize: {}", SDL_GetError());
        exit(1);
	}

    if (!SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" )){
        LOGGGER_WARNING("Linear texture filtering not enabled.");
    }

    if (IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG){
       LOGGGER_ERROR("SDL_image could not initialize: {}", IMG_GetError());
        exit(1);
    }

    if(TTF_Init() == -1){
       LOGGGER_ERROR("SDL_ttf could not initialize: {}", TTF_GetError());
        exit(1);
    }

    SDL_Window* raw_window = SDL_CreateWindow( "Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
    if (raw_window == NULL){
       LOGGGER_ERROR("Window could not be created: {}", SDL_GetError());
        exit(1);
    }
    window = std::unique_ptr<SDL_Window, SDL_window_deleter>(raw_window, SDL_window_deleter());

    SDL_Renderer* raw_renderer = SDL_CreateRenderer(window.get(), -1, SDL_RENDERER_ACCELERATED);
    if (raw_renderer == NULL){
        printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
       LOGGGER_ERROR("Renderer could not be created: {}", SDL_GetError());
        exit(1);
    }
    renderer = std::unique_ptr<SDL_Renderer, SDL_renderer_deleter>(raw_renderer, SDL_renderer_deleter());
}

SDL::~SDL(){
    LOGGGER_TRACE("SDL::~SDL()");
    textures.clear();
    texts.clear();
    fonts.clear();

    window.reset();
    renderer.reset();
    
    IMG_Quit();
    SDL_Quit();
    TTF_Quit();
}

void SDL::start_game(
    std::function<void(SDL* sdl)> render_callback,
    std::function<bool(SDL* sdl, const SDL_Event& event, float delta_time)> input_callback 
)
{
    uint32_t frame_start, frame_time;

    int frame_count = 0;
    int start_time = SDL_GetTicks();

    bool quit = false;
    while (!quit) {
        frame_start = SDL_GetTicks();

        static int last_fram_start = frame_start;
        float delta_time = (frame_start - last_fram_start) / 1000.0f;
        last_fram_start = frame_start;

        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                quit = true;
                break;
            }
            input_callback(this, event, delta_time);
        }
        SDL_RenderClear(renderer.get());

        render_callback(this);
        render_static_texts();
        
        SDL_RenderPresent(renderer.get());

        frame_time = SDL_GetTicks() - frame_start; 

        if (frame_time < FRAME_DELAY) {
            SDL_Delay(FRAME_DELAY - frame_time);
        }

        frame_count ++;
        uint32_t cur_time = SDL_GetTicks();
        uint32_t elapsed_time = cur_time - start_time;
        if (elapsed_time >= 1000) {
            fps = static_cast<float>(frame_count) / (elapsed_time / 1000.0f);

            frame_count = 0;
            start_time = cur_time;
        }
    }
}

void SDL::load_textures(std::string folder){
    for (auto file : Common::get_file_names(folder)) {
        SDL_Texture* raw_texture = load_texture(folder + "/" + file, renderer.get());
        if (raw_texture == nullptr) {
           LOGGGER_ERROR("Failed to load texture {}", file);
            continue;
        }
        std::string extension = file.substr(file.find_last_of(".") + 1);
        if (extension != "png") {
            LOGGGER_DEBUG("Not loading {} as texture", file);
            continue;
        }
        std::string no_extension = file.substr(0, file.find_last_of("."));
        if (textures.find(no_extension) != textures.end()) {
            LOGGGER_WARNING("Texture {} already loaded", no_extension);
            continue;
        }
        LOGGGER_DEBUG("Adding texture {}", no_extension);
        textures[no_extension] = std::unique_ptr<SDL_Texture, SDL_texture_deleter>(raw_texture, SDL_texture_deleter());
    }
}

int SDL::add_static_text(const std::string &text, int x, int y, bool word_for_word, const std::string &font, uint8_t r, uint8_t g, uint8_t b){
    SDL_Color color = {r, g, b};
    SDL_Surface* surface = TTF_RenderText_Solid(fonts[font].get(), text.c_str(), color);
    if (surface == NULL){
       LOGGGER_ERROR("Failed to create text surface: {}", TTF_GetError());
        return -1;
    }

    SDL_Texture* raw_texture = SDL_CreateTextureFromSurface(renderer.get(), surface);
    if (raw_texture == NULL){
       LOGGGER_ERROR("Failed to create texture from surface: {}", SDL_GetError());
        SDL_FreeSurface(surface);
        return -1;
    }

    int free_key = 1;
    while (texts.find(free_key) != texts.end()) {
        free_key++;
    }
    LOGGGER_DEBUG("Setting text {} at key {}", text, free_key);
    if (!word_for_word){
        texts[free_key] = std::make_unique<Text>(raw_texture, SDL_Rect{x, y, surface->w, surface->h});
    } else {
        texts[free_key] = std::make_unique<WordForWordText>(raw_texture, SDL_Rect{x, y, surface->w, surface->h}, text, font);
    }
    SDL_FreeSurface(surface);
    return free_key;
}

bool SDL::remove_text(int key){
    if (texts.find(key) == texts.end()) {
        LOGGGER_WARNING("Cannot delete text with ID {}, text not found", key);
        return false;
    }
    LOGGGER_DEBUG("Deleting text with ID {}", key);
    texts.erase(key);
    return true;
}

float SDL::get_fps() const{
    return fps;
}

void SDL::set_view_port(int x, int y, int w, int h)
{
    // This adds overhead over creating rect each timer
    // We can preset them and map them, but it if proves to be slow we can always do that
    SDL_Rect viewport = SDL_Rect{x, y, w, h};
    SDL_RenderSetViewport(renderer.get(), &viewport);
}

bool SDL::add_font(const std::string name, const std::string &path, int ptsize)
{
    LOGGGER_TRACE("Adding font {} with ptsize {}", path, ptsize);

    if (fonts.find(name) != fonts.end()) {
        LOGGGER_WARNING("Font {} already loaded", path);
        return false;
    }

    TTF_Font* raw_font = TTF_OpenFont(path.c_str(), ptsize);
    if (raw_font == NULL){
       LOGGGER_ERROR("Failed to load font: {}", TTF_GetError());
        return false;
    }
    fonts[name] = std::unique_ptr<TTF_Font, TTF_font_deleter>(raw_font, TTF_font_deleter());
    return true;
}

void SDL::render_sprite(Sprite& sprite, int x, int y, int w, int h)
{
    SDL_Rect src = SDL_Rect{sprite.getX(), sprite.getY(), sprite.getW(), sprite.getH()};
    SDL_Rect dst = SDL_Rect{x, y, w, h};
    SDL_RenderCopy(renderer.get(), textures[sprite.get_texture_str()].get(), &src, &dst);
    sprite.tick();
}

void SDL::render_text(const std::string &text, int x, int y, const std::string &font, uint8_t r, uint8_t g, uint8_t b)
{
    SDL_Color color = {r, g, b};
    SDL_Surface* surface = TTF_RenderText_Solid(fonts[font].get(), text.c_str(), color);
    if (surface == NULL){
       LOGGGER_ERROR("Failed to create text surface: {}", TTF_GetError());
        return;
    }

    SDL_Texture* raw_texture = SDL_CreateTextureFromSurface(renderer.get(), surface);
    if (raw_texture == NULL){
       LOGGGER_ERROR("Failed to create texture from surface: {}", SDL_GetError());
        SDL_FreeSurface(surface);
        return;
    }

    SDL_Rect pos = {x, y, surface->w, surface->h};
    SDL_RenderCopy(renderer.get(), raw_texture, NULL, &pos);
    SDL_DestroyTexture(raw_texture);
    SDL_FreeSurface(surface);
}

void SDL::render_static_texts()
{
    for (auto& pair : texts) {
        const std::unique_ptr<Text>& text = pair.second;
        SDL_Rect* dst = text->get_pos_raw();
        SDL_Rect src = {0, 0, dst->w, dst->h};
        //dst->w = text->w;
        SDL_RenderCopy(renderer.get(), text->texture.get(), &src, dst);
        text->tick();
    }
}