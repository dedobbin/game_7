#include <sstream>
#include "text.h"
#include "sdl.h"
Text::Text(SDL_Texture* texture, SDL_Rect pos)
: texture(std::unique_ptr<SDL_Texture, SDL_texture_deleter>(texture, SDL_texture_deleter())), pos(pos)
{
    //TTF_SizeText(SDL::get()->fonts["default"].get(), text.c_str(), &pos.w, &pos.h);
}

void Text::tick(){
    //do nothing
}

SDL_Rect* Text::get_pos_raw(){
    return &pos;
}

WordForWordText::WordForWordText(SDL_Texture* texture, SDL_Rect pos, std::string text, std::string font)
: Text(texture, pos), original_w (pos.w), text(text), font(font){
    this->pos.w = 0;
    next_word();
}

int WordForWordText::next_word(){
    if (pos.w >= original_w) {
        // Avoid calculating the obvious
        return original_w;
    }
    size_t space_pos = text.find(' ', cur_text.size()+1);
    cur_text = text.substr(0, space_pos);
    if (cur_text.size() != text.size()) {
        cur_text += " ";
    }
    return TTF_SizeText(SDL::get()->fonts[font].get(), cur_text.c_str(), &pos.w, &pos.h);
}

void WordForWordText::tick(){
    if (cur_tick >= ticks_per_word) {
        cur_tick = 0;
        next_word();
    }
    cur_tick ++;
}