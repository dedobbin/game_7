#include <algorithm>
#include "loggger.h"
#include "sprite.h"

Sprite::Sprite (std::string texture_str, int x, int y, int w, int h) :
    texture_str(texture_str), x(x), y(y), w(w), h(h)
{}

Sprite::Sprite(std::string texture_str, std::vector<AnimationFrame> animation_frames) :
    texture_str(texture_str), animation_frames(animation_frames)
{
    if (animation_frames.size() == 0) {
        LOGGGER_WARNING("Sprite {} has no animation frames", texture_str);
    } else {
        x = animation_frames[0].x;
        y = animation_frames[0].y;
        w = animation_frames[0].w;
        h = animation_frames[0].h;
    }
}

Sprite::Sprite(){
    x = -1;
    y = -1;
    w = -1;
    h = -1;
}

Sprite& Sprite::operator=(const Sprite &other)
{
       if (this == &other) {
            return *this; // Handle self-assignment
        }

        x = other.x;
        y = other.y;
        w = other.w;
        h = other.h;
        cur_frame_animation = other.cur_frame_animation;
        texture_str = other.texture_str;
        animation_frames.clear();
        for (auto frame : other.animation_frames) {
            animation_frames.push_back(frame);
        }
        return *this;
}

void Sprite::tick()
{
    if (animation_frames.size() == 0) {
        return;
    }
    int dur = animation_frames[cur_frame_animation].duration;
    if (cur_frame_tick >= dur){
        cur_frame_tick = 0;
        cur_frame_animation = (cur_frame_animation + 1) % animation_frames.size();
        x = animation_frames[cur_frame_animation].x;
        y = animation_frames[cur_frame_animation].y;
        w = animation_frames[cur_frame_animation].w;
        h = animation_frames[cur_frame_animation].h;
    }
    cur_frame_tick++;
    int w = 4;
}

std::string Sprite::get_texture_str() const
{
    return texture_str;
}   

int Sprite::getX() const
{
    return x;
}

int Sprite::getY() const
{
    return y;
}

int Sprite::getW() const
{
    return w;
}

int Sprite::getH() const
{
    return h;
}