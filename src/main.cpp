#include "sdl.h"
#include "entity.h"
#include <iostream>

int main(int argc, char** argv)
{
    auto sdl = SDL::get();
    sdl->load_textures("../assets/spritesheets");

    std::unordered_map<std::string, Sprite> sprites = {
        {"default", Sprite("not_megaman", 10, 0, 40, 90)},
        {"walking", Sprite("not_megaman", {
            {10, 0, 40, 90, 10},
            {100, 0, 40, 90, 10},
        })},
    };

    std::vector entities = {
        Entity(sprites, 10, 10, 40, 90),
    };
    
    auto render_handler = [&entities](SDL *sdl){
       for (auto& entity : entities) {
            int x, y, w, h;
            std::tie(x, y, w, h) = entity.get_pos();
            sdl->render_sprite(entity.sprite, x, y, w, h);
            sdl->render_text(std::to_string(sdl->get_fps()), 10, 10);
        }
    };

    auto input_handler = [&entities](SDL *sdl, const SDL_Event& event, float delta_time){        
        if (event.type == SDL_KEYDOWN) {
            switch (event.key.keysym.sym) {
                case SDLK_SPACE:
                    entities[0].set_active_sprite("walking");
                    break;
                case SDLK_DOWN:
                    entities[0].move(Dir::DOWN, delta_time);
                    break;
                case SDLK_UP:
                    entities[0].move(Dir::UP, delta_time);
                    break;
                case SDLK_LEFT:
                    entities[0].move(Dir::LEFT, delta_time);
                    break;
                case SDLK_RIGHT:
                    entities[0].move(Dir::RIGHT, delta_time);
                    break;
                default:
                    break;
            }
        }
        return true;
    };

    //sdl.set_view_port(200, 200, 20, 20);
    sdl->add_font("default", "../assets/font.ttf", 28);
    //sdl.add_static_text("this is a funny text", 10, 10);
    sdl->add_static_text("aaa bbb ccc ddd", 10, 40, true);
    sdl->start_game(render_handler, input_handler);
}