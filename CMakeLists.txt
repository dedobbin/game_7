cmake_minimum_required(VERSION 3.10)

project(handrolled)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")

#sudo apt install cmake libsdl2-dev
find_package(SDL2 REQUIRED)
#sudo apt install libsdl2-image-dev
find_package(SDL2_image REQUIRED)
#sudo apt install libsdl2-ttf-dev
find_package(SDL2_ttf REQUIRED)

add_subdirectory(submodules/loggger)

include_directories(
    inc
    ${SDL2_INCLUDE_DIRS}
    ${SDL2_IMAGE_INCLUDE_DIRS}
    ${SDL2_TTF_INCLUDE_DIRS}
)

set(CMAKE_BUILD_TYPE Debug)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_C_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(${PROJECT_NAME} 
    src/main.cpp
    src/sdl.cpp
    src/entity.cpp
    src/sprite.cpp
    src/sdl_deleters.cpp
    src/text.cpp
)

add_definitions(-DLOGGGER_LEVEL=6)

set_target_properties(${PROJECT_NAME} PROPERTIES OUTPUT_NAME "program")
target_link_libraries(${PROJECT_NAME} PRIVATE 
    ${SDL2_LIBRARIES} 
    ${SDL2_IMAGE_LIBRARIES}
    ${SDL2_TTF_LIBRARIES}
    Loggger
)

